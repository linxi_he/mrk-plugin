#include <functional>
#include <string>
#include <vector>
#include <ignition/math.hh>
#include "ignition/math/Vector3.hh"
#include "gazebo/common/Plugin.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/util/system.hh"
#include "iostream"
using namespace std;



namespace gazebo
{    
    class actor_plugin : public ModelPlugin
    {
        public: void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
        {
            this->sdf = _sdf;
            this->actor = boost::dynamic_pointer_cast<physics::Actor>(_model); //get the parent element
            this->world = this ->actor->GetWorld(); //get world element
            
            if (this->sdf && this->sdf->HasElement("target"))
                this->target = this->sdf->Get<ignition::math::Vector3d>("target");// get the pose of target. store in target
            else
                this->target = ignition::math::Vector3d(0, -5, 1.2138);

            auto skelAnims = this->actor->SkeletonAnimations(); //get skeletonanimation and store in skelAnims
            if (skelAnims.find("walking") == skelAnims.end()) //find walking animation
                {
                    gzerr << "Skeleton animation walk not found.\n";
                }
            else
                {
            // Create custom trajectory
                    this->trajectoryInfo.reset(new physics::TrajectoryInfo()); //reset a new trajectoryInfo
                    this->trajectoryInfo->type = "walking";
                    this->trajectoryInfo->duration = 10.0; //持续时间为1.0s

                    this->actor->SetCustomTrajectory(this->trajectoryInfo);
                }   


            //update loop
            this->velocity = 0.1;

            //ignition::math::Pose3d pose(0, 5, 1, 1.57, 0, 3.14);

            //this->actor->SetWorldPose(pose, false, false);//设置在世界中的新位置

            //double distanceTraveled = (pose.Pos() - this->actor->WorldPose().Pos()).Length();

            //从当前脚本循环开始的时间（以秒为单位）
            //this->actor->SetScriptTime(this->actor->ScriptTime() + (distanceTraveled * this->animationFactor));

            //从当前脚本循环开始的时间（以秒为单位）
            
            //this->lastUpdate = _info.simTime;
            this->connections.push_back(event::Events::ConnectWorldUpdateBegin(
          std::bind(&actor_plugin::OnUpdate, this, std::placeholders::_1)));
            //this->Reset();
        }
        public: void Reset()
        {
            this->velocity = 0.1;
            this->lastUpdate = 0;

            if (this->sdf && this->sdf->HasElement("target"))
                this->target = this->sdf->Get<ignition::math::Vector3d>("target");// get the pose of target. store in target
            else
                this->target = ignition::math::Vector3d(0, -5, 1.2138);
        
            //获取骨骼动画
            auto skelAnims = this->actor->SkeletonAnimations(); //get skeletonanimation and store in skelAnims
            if (skelAnims.find("walking") == skelAnims.end()) //find walking animation
                {
                    gzerr << "Skeleton animation walk not found.\n";
                }
            else
                {
            // Create custom trajectory
                    this->trajectoryInfo.reset(new physics::TrajectoryInfo()); //reset a new trajectoryInfo
                    this->trajectoryInfo->type = "walking";
                    this->trajectoryInfo->duration = 1.0; //持续时间为1.0s

                    this->actor->SetCustomTrajectory(this->trajectoryInfo);
                }             
        }

        public: void OnUpdate(const common::UpdateInfo &_info)
        {
            // Time delta dt=当前仿真时间
            double dt = (_info.simTime - this->lastUpdate).Double();

            ignition::math::Pose3d pose = this->actor->WorldPose(); // pose ------actor目前位置
            ignition::math::Vector3d pos = this->target - pose.Pos(); //pos -------actor距离目标的相对位置
            ignition::math::Vector3d rpy = pose.Rot().Euler(); //rpy -------------距离目标的方位角
            //cout << pose << endl;
            double distance = pos.Length(); //计算距离

            // Compute the yaw orientation 计算偏航方向
            ignition::math::Angle yaw = atan2(pos.Y(), pos.X()) + 1.5707 - rpy.Z();
            yaw.Normalize();

            if (std::abs(yaw.Radian()) > IGN_DTOR(10))
            {
                pose.Rot() = ignition::math::Quaterniond(1.5707, 0, rpy.Z()+
                yaw.Radian()*0.001);
            }
            else
            {
                pose.Pos() += pos * this->velocity * dt; //actor新位置= 旧的位置+速度×时间
                pose.Rot() = ignition::math::Quaterniond(1.5707, 0, rpy.Z()+yaw.Radian()); //新的角度==偏航方向
            }

            double distanceTraveled = (pose.Pos() - this->actor->WorldPose().Pos()).Length();

            this->actor->SetWorldPose(pose, false, false);//设置在世界中的新位置
            //从当前脚本循环开始的时间（以秒为单位）
            this->actor->SetScriptTime(this->actor->ScriptTime() + (distanceTraveled * this->animationFactor));
            cout << this->actor->ScriptTime()<< endl;
            cout << distanceTraveled << endl;
            this->lastUpdate = _info.simTime;
        }


        private: physics::ActorPtr actor;

        //Pointer to the world
        private: physics::WorldPtr world;

        //Pointer to the sdf element.
        private: sdf::ElementPtr sdf;

        // Velocisy of the actor
        private: ignition::math::Vector3d velocity;

        //List of connections;
        private: std::vector<event::ConnectionPtr> connections;

        //Current target location
        private: ignition::math::Vector3d target;

        //Targe location weit
        private: double targetWeight = 1.0;

        //Obstacle weight
        private: double obstacleWeight =1.0;

        //Time scaling factor. 
        private: double animationFactor =4;

        //Time of the last update
        private: common::Time lastUpdate;

        //List of models to ignore.
        private: std::vector<std::string> ignoreModels;

        //Custon trajectory info
        private: physics::TrajectoryInfoPtr trajectoryInfo;
    };
    GZ_REGISTER_MODEL_PLUGIN(actor_plugin);
}       

