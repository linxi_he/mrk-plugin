#include <functional>
#include <string>
#include <vector>
#include <ignition/math.hh>
#include "ignition/math/Vector3.hh"
#include "gazebo/common/Plugin.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/util/system.hh"
#include "iostream"
#include <ignition/math/Quaternion.hh>
#include <ignition/math/Vector3.hh>

#include "gazebo/common/Animation.hh"
#include "gazebo/common/CommonTypes.hh"
#include "gazebo/common/KeyFrame.hh"
#include "gazebo/gazebo.hh"

using namespace std;



namespace gazebo
{    
    class actor_pose : public ModelPlugin
    {
        public: void Load(physics::ModelPtr _model, sdf::ElementPtr /*_sdf*/)
        {
            this->actor = boost::dynamic_pointer_cast<physics::Actor>(_model); //get the parent element
            this->world = this ->actor->GetWorld();

            gazebo::common::PoseAnimationPtr anim(
            new gazebo::common::PoseAnimation("walking", 5, true));

            gazebo::common::PoseKeyFrame *key;

            key = anim->CreateKeyFrame(0);
            key->Translation(ignition::math::Vector3d(0, 0, 1.2));
            key->Rotation(ignition::math::Quaterniond(0, 0, 0));

            key = anim->CreateKeyFrame(5.0);
            key->Translation(ignition::math::Vector3d(0, 5, 1.2));
            key->Rotation(ignition::math::Quaterniond(0, 0, 0));

            this->actor->SetAnimation(anim);

            //auto skelAnims = this->actor->SkeletonAnimations(); //get skeletonanimation and store in skelAnims
           // if (skelAnims.find("walking") == skelAnims.end()) //find walking animation
           //     {
           //         gzerr << "Skeleton animation walk not found.\n";
           //     }
           // else
            //    {
            // Create custom trajectory
           this->trajectoryInfo.reset(new physics::TrajectoryInfo()); //reset a new trajectoryInfo
           this->trajectoryInfo->type = "walking";
           this->trajectoryInfo->duration = 1.0; //持续时间为1.0s
            //        this->trajectoryInfo->startTime = 0;
            //        this->trajectoryInfo->endTime = 10.0;
           this->actor->SetCustomTrajectory(this->trajectoryInfo);
                  
            //    }   
                //common::Time currentTime = this->world->SimTime();

                //this->actor->SetScriptTime(currentTime.Double());



            
        }

        private: physics::ActorPtr actor;
        private: physics::TrajectoryInfoPtr trajectoryInfo;
        private: physics::WorldPtr world;
        private: common::Time currentTime;
    };
    GZ_REGISTER_MODEL_PLUGIN(actor_pose);
}